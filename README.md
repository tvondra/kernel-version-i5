PostgreSQL vs. kernel versions
==============================

This repository contains tools (well, a bunch of shell scripts) for running
a suite of PostgreSQL benchmarks using pgbench, and results from multiple
Linux kernel versions.


benchmark scripts
-----------------

The following two scripts are the primary benchmarking tools:

* run-pgbench-short.sh - reinitializes the cluster only once for each scale,
  runs a suite of read-only/read-write pgbench runs for multiple scales

* run-pgbench-long.sh - only runs a single long read-write benchmark for
  each scale, with a single client count (no read-only runs etc.)

Running the scripts is fairly simple - both scripts accept the directory for
results as a single parameter, and print a simple progress log. So create
a directory 'results' and run the script (storing the results in the directory)
like this:

    $ mkdir results
    $ ./run-pgbench-short.sh ./results > ./results/bench.log 2>&1


directory structure
-------------------

The repository contains a number of directories matching kernel version,
with a variety of information shared by all the runs:

* PostgreSQL log (pg.log)
* cpuinfo
* sysctl info
* list of mounts
* pg_settings data
* postgresql.conf
* dum of sar data
* kernel config

and also results for all the runs, orgainzed in a simple directory structure,
with $scale/$run directories. For each run the directory contains log files
for each type of benchmark (read-only/read-write) and client count. It also
includes pgbench transaction log files (aggregated per second) etc.


result processing
-----------------

To make analysis easier, the repository also includes shell scripts extracting
results as CSV (printed on stdout).

* build-long.sh - throughput info (tps, avg latency) for long runs
* build-short.sh - throughput info (tps, avg latency) for short runs
* build-histograms-long.sh - histogram/percentiles from aggregated data
* build-histograms-short.sh - histogram/percentiles from aggregated data

So for example:

    $ ./build-short.sh > short-results.csv

should do the trick.
