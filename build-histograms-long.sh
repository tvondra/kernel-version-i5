for k in 2.6.39-long 3.0.101-long 3.2.81-long 3.4.112-long 3.10.102-long 3.12.61-long 3.14.73-long 3.16.36-long 3.18.38-long 4.1.29-long 4.4.16-long 4.6.5-long 4.7-long; do

	for s in 30 300 1500; do

		if [ -f "$k/$s/read-write.log" ]; then

			tar -xf $k/$s/read-write.tgz -O | ./build-histograms.py "$k $s $r $c $t" .05 .25 0.50 .75 .95 .99

		fi

	done

done
