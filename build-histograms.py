#!/usr/bin/python

import math
import sys

lines = [l.strip().split(' ') for l in sys.stdin.readlines()]
percentiles = [float(v) for v in sys.argv[2:]]

tps      = [int(l[1]) for l in lines]
averages = [int(float(l[2]) / int(l[1])) for l in lines if int(l[1]) != 0]
maximums = [int(l[5]) for l in lines if int(l[1]) != 0]
stddevs  = [int(math.sqrt((int(l[1]) * float(l[3]) - float(l[2]) * float(l[2])) / (int(l[1]) * int(l[1])))) for l in lines if int(l[1]) != 0]

tps      = sorted(tps)
averages = sorted(averages)
maximums = sorted(maximums)
stddevs  = sorted(stddevs)

# percentiles
print sys.argv[1], 'tps', ' '.join([str(tps[int(p * len(tps) - 1)]) for p in percentiles])
print sys.argv[1], 'average', ' '.join([str(averages[int(p * len(averages) - 1)]) for p in percentiles])
print sys.argv[1], 'maximum', ' '.join([str(maximums[int(p * len(averages) - 1)]) for p in percentiles])
print sys.argv[1], 'stddev', ' '.join([str(stddevs[int(p * len(averages) - 1)]) for p in percentiles])
