for k in 2.6.39-long 3.0.101-long 3.2.81-long 3.4.112-long 3.10.102-long 3.12.61-long 3.14.73-long 3.16.36-long 3.18.38-long 4.1.29-long 4.4.16-long 4.6.5-long 4.7-long; do

	for s in 30 300 1500; do

		if [ -f "$k/$s/read-write.log" ]; then

			tps=`grep 'excluding' "$k/$s/read-write.log" | awk '{print $3}'`

			latency=`grep 'latency average' "$k/$s/read-write.log" | awk '{print $3}'`

			echo $k $s $t $tps $latency

		fi	

	done

done
