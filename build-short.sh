for k in 2.6.39 2.6.39-deadline 2.6.39-noop 3.0.101 3.2.81 3.4.112 3.10.102 3.10.102-deadline 3.10.102-noop 3.10.102-sample 3.12.61 3.12.61-sample 3.14.73 3.16.36 3.16.36-deadline 3.16.36-noop 3.18.38 4.1.29 4.4.16 4.6.5 4.7 4.7-deadline 4.7-noop; do

	for s in 30 300 1500; do

		for r in 1 2 3; do

			for c in 1 4 16; do

				for t in 'read-only' 'read-write'; do

					if [ -f "$k/$s/$r/$t-$c.log" ]; then

						tps=`grep 'excluding' "$k/$s/$r/$t-$c.log" | awk '{print $3}'`

						latency=`grep 'latency average' "$k/$s/$r/$t-$c.log" | awk '{print $3}'`

						echo $k $s $r $c $t $tps $latency

					fi

				done

			done

		done

	done

done
