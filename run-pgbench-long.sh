DURATION_RW=21600 # 6-hour runs r/w
OUTDIR=$1
CLIENTS=16

function log {
	echo `date +%s` `date +"%Y-%m-%d %H:%M:%S"` $1
}

sudo rm /mnt/data/sysstat/*
sleep 60

psql postgres -c "select * from pg_settings" > pg_settings.log
pg_config > pg_config.log
cat /proc/cpuinfo > cpuinfo.log
dmesg > dmesg.log
mount > mount.log
sudo sysctl -a > sysctl.log

# three scales 30 (450MB), 300 (4.5GB), 1500 (22GB)
for s in 30 300 1500; do

	outdir="$OUTDIR/$s/$r"
	mkdir -p $outdir

	log "pgbench scale=$s : start"

	# recreate the db
	dropdb --if-exists pgbench > /dev/null 2>&1
	createdb pgbench > /dev/null 2>&1

	log "pgbench scale=$s : init"

	# initialize
	pgbench -i -s $s -q pgbench > /dev/null 2>&1

	# do a checkpoint first
	psql pgbench -c "checkpoint" > /dev/null 2>&1

	log "pgbench scale=$s : read-write"

	# read-write test
	psql postgres -c "select * from pg_stat_bgwriter" > $outdir/bgwriter.before.log 2>&1
	psql postgres -c "select * from pg_stat_database" > $outdir/database.before.log 2>&1

        # warmup (read-only)
	pgbench -c $CLIENTS -l --aggregate-interval=1 -T $DURATION_RW pgbench > $outdir/read-write.log 2>&1

        psql postgres -c "select * from pg_stat_bgwriter" > $outdir/bgwriter.after.log 2>&1
        psql postgres -c "select * from pg_stat_database" > $outdir/database.after.log 2>&1

	# pack the transaction log
	tar -czf $outdir/read-write.tgz pgbench_log.*
	rm pgbench_log.*

	log "pgbench scale=$s : done"

done
