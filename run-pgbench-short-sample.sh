DURATION_RW=1800 # 30-minute runs r/w
DURATION_RO=900  # 15-minute runs r/o
RUNS=3
CLIENTS="1 4 16"
OUTDIR=$1
SAMPLING_RATE=0.05

function log {
	echo `date +%s` `date +"%Y-%m-%d %H:%M:%S"` $1
}

sudo rm /mnt/data/sysstat/*
sleep 60

psql postgres -c "select * from pg_settings" > pg_settings.log
pg_config > pg_config.log
cat /proc/cpuinfo > cpuinfo.log
dmesg > dmesg.log
mount > mount.log
sudo sysctl -a > sysctl.log
sudo mdadm --detail "/dev/md/bench:data" > mdadm.log 2>&1

for d in sda sdb sdc sdd sde sdf; do
	echo $d >> schedulers.txt;
	cat /sys/block/$d/queue/scheduler >> schedulers.txt;
done;

# three scales 30 (450MB), 300 (4.5GB), 1500 (22GB)
for s in 30 300 1500; do

	outdir="$OUTDIR/$s"
	mkdir -p $outdir

	log "pgbench scale=$s"

	# recreate the db
	dropdb --if-exists pgbench > /dev/null 2>&1
	createdb pgbench > /dev/null 2>&1

	log "pgbench scale=$s : init"

	# initialize
	pgbench -i -s $s -q pgbench > /dev/null 2>&1

	log "pgbench scale=$s : warmup"

	psql postgres -c "select * from pg_stat_bgwriter" > $outdir/warmup.bgwriter.before.log 2>&1
	psql postgres -c "select * from pg_stat_database" > $outdir/warmup.database.before.log 2>&1

	# warmup (read-only)
	pgbench -S -c 4 -l --sampling-rate=$SAMPLING_RATE -T $DURATION_RO pgbench > $outdir/warmup.log 2>&1

	psql postgres -c "select * from pg_stat_bgwriter" > $outdir/warmup.bgwriter.after.log 2>&1
	psql postgres -c "select * from pg_stat_database" > $outdir/warmup.database.after.log 2>&1

	# pack the transaction log
	tar -czf $outdir/warmup.tgz pgbench_log.*
	rm pgbench_log.*

	# three runs for each combination
	for r in `seq 1 $RUNS`; do

		outdir="$OUTDIR/$s/$r"
		mkdir -p $outdir

		log "pgbench scale=$s run=$r"

		# a few basic client counts
		for c in $CLIENTS; do

			# do a checkpoint first
			psql pgbench -c "checkpoint" > /dev/null 2>&1

			log "pgbench scale=$s run=$r clients=$c : read-only"

			# read-only test
			psql postgres -c "select * from pg_stat_bgwriter" > $outdir/read-only-$c.bgwriter.before.log 2>&1
			psql postgres -c "select * from pg_stat_database" > $outdir/read-only-$c.database.before.log 2>&1

			pgbench -S -c $c -l --sampling-rate=$SAMPLING_RATE -T $DURATION_RO pgbench > $outdir/read-only-$c.log 2>&1

			psql postgres -c "select * from pg_stat_bgwriter" > $outdir/read-only-$c.bgwriter.after.log 2>&1
			psql postgres -c "select * from pg_stat_database" > $outdir/read-only-$c.database.after.log 2>&1

			# pack the transaction log
			tar -czf $outdir/read-only-$c.tgz pgbench_log.*
			rm pgbench_log.*

			# do a checkpoints first
			psql pgbench -c "checkpoint" > /dev/null 2>&1

			log "pgbench scale=$s run=$r clients=$c : read-write"

			# read-write test
			psql postgres -c "select * from pg_stat_bgwriter" > $outdir/read-write-$c.bgwriter.before.log 2>&1
			psql postgres -c "select * from pg_stat_database" > $outdir/read-write-$c.database.before.log 2>&1

			pgbench -c $c -l --sampling-rate=$SAMPLING_RATE -T $DURATION_RW pgbench > $outdir/read-write-$c.log 2>&1

			psql postgres -c "select * from pg_stat_bgwriter" > $outdir/read-write-$c.bgwriter.after.log 2>&1
			psql postgres -c "select * from pg_stat_database" > $outdir/read-write-$c.database.after.log 2>&1

			# pack the transaction log
			tar -czf $outdir/read-write-$c.tgz pgbench_log.*
			rm pgbench_log.*

		done

		log "pgbench scale=$s clients=$c run=$r : done"

	done

done
